package com.example.practica02imckotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var txtAltura: EditText
    private lateinit var txtPeso: EditText
    private lateinit var lblIMC: TextView
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar los objetos
        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        lblIMC = findViewById(R.id.lblIMC)
        btnCalcular = findViewById(R.id.btnCalcularIMC)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        // Codificar el evento clic del botón Calcular IMC
        btnCalcular.setOnClickListener {
            // Validar inputs vacíos
            if(txtAltura.text.toString().isEmpty()) {
                Toast.makeText(this, "Campo de altura vacío", Toast.LENGTH_SHORT).show()
            }else if(txtPeso.text.toString().isEmpty()) {
                Toast.makeText(this, "Campo de peso vacío", Toast.LENGTH_SHORT).show()
            } else {
                // Obtener los valores decimales de altura y peso
                val altura = txtAltura.text.toString().toDouble()
                val peso = txtPeso.text.toString().toDouble()

                // Validar los valores ingresados
                if(altura <= 0 || peso <= 0){
                    Toast.makeText(this, "Ingrese valores válidos", Toast.LENGTH_SHORT).show()
                }else {
                    // Calcular el IMC y convertir la altura de cm a m
                    val alturaM = altura / 100
                    val imc = peso / (alturaM * alturaM)
                    // Establecer un formato de dos decimales
                    val df = DecimalFormat("#.00")

                    // Mostrar el resultado del IMC del usuario
                    lblIMC.text = ("Resultado IMC: ${df.format(imc)} kg/m2")

                }
            }
        }

        // Codificar el evento clic del botón Limpiar
        btnLimpiar.setOnClickListener {
            // Validar inputs vacíos
            if(txtAltura.text.toString().isEmpty() && txtPeso.text.toString().isEmpty()){
                Toast.makeText(this, "Los campos ya están vacíos", Toast.LENGTH_SHORT).show()
            } else {
                // Se limpian los campos y etiquetas
                txtAltura.setText("")
                txtAltura.setHint("Altura")
                txtPeso.setText("")
                txtPeso.setHint("Peso")
                lblIMC.text = "Resultado IMC:"
            }
        }

        // Codificar el evento clic del botón Cerrar
        btnCerrar.setOnClickListener {
            finish()
        }
    }
}